import socket
import threading
import time
import zlib


HOST = "0.0.0.0"
PORT = 8787
FIRST = 0


class troller_proxy(object):
    def __init__(self):
        self._server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._server.bind((HOST, PORT))

    def listen(self):
        self._server.listen(5)
        while True:
            client, address = self._server.accept()
            client.settimeout(60)
            threading.Thread(target=self.handle_client, args=(client, address)).start()

    def handle_client(self, client, address):
        global FIRST
        version, supported, method = self.greeting_from_client(client, address)
        if FIRST == 0:
            FIRST = 1
            return None
        else:
            FIRST = 0

        print version, supported, method, time.time()

        client.send("{}{}".format(chr(5), chr(0)))
        print repr(__name__ + "sending server choice {}{}".format(chr(5), chr(0))), time.time()

        destination, port, decoded_port = self.client_connection_request(client, address)
        print destination, port, decoded_port, time.time()

        kosher_client = socket.socket()
        kosher_client.connect((destination, port))

        self.server_response(client, address)

        a = threading.Thread(target=self.wait_and_send, args=(kosher_client, client)).start()
        b = threading.Thread(target=self.wait_and_send, args=(client, kosher_client)).start()


    def greeting_from_client(self, client, address):
        socks_version = ord(client.recv(1))
        authentication_methods_supported = ord(client.recv(1))
        authentication_method = ord(client.recv(1))
        return socks_version, authentication_methods_supported, authentication_method

    def client_connection_request(self, client, address):
        socks_version = ord(client.recv(1))
        command_code = ord(client.recv(1))
        reserved = ord(client.recv(1))
        address_type = ord(client.recv(1))
        iped = False
        if address_type == 1:  #IP
            iped = True
            st = str(ord(client.recv(1)))
            nd = str(ord(client.recv(1)))
            rd = str(ord(client.recv(1)))
            th = str(ord(client.recv(1)))
            ip = '{}.{}.{}.{}'.format(st, nd, rd, th)

        elif address_type == 3:  #DOMAIN
            length = ord(client.recv(1))
            domain = client.recv(length)
        else:
            raise

        first_port_decoded = client.recv(1)
        second_port_decoded = client.recv(1)

        first_port = str(bin(ord(first_port_decoded)))[2:]
        second_port = str(bin(ord(second_port_decoded)))[2:]

        port = int(first_port + second_port, 2)

        if iped:
            return ip, port, [first_port_decoded, second_port_decoded]
        return domain, port, [first_port_decoded, second_port_decoded]

    def server_response(self, client, address):
        message = "{}{}{}{}{}{}{}{}{}{}".format(chr(5), chr(0), chr(0), chr(1), chr(0), chr(0), chr(0), chr(0), chr(0),
                                                chr(0))

        client.send(message)
        print repr(
            __name__ + " sent {}{}{}{}{}{}{}{}{}{}".format(chr(5), chr(0), chr(0), chr(1), chr(0), chr(0), chr(0),
                                                           chr(0), chr(0), chr(0)))

    def wait_and_send(self, my_sock, target_sock):
        while True:
            try:
                data = my_sock.recv(1024)
                if data != "":
                    data = data.replace("Accept-Encoding: gzip, deflate", "Accept-Encoding: identity;q=0")
                    data = data.replace("Content-Encoding: gzip", "Content-Encoding: identity;q=0")
                    pos = data.find("<html>")
                    html = data[pos:]
                    change
                    print data
                    target_sock.send(data)
            except:
                continue



if __name__ == '__main__':
    s = troller_proxy()
    s.listen()
